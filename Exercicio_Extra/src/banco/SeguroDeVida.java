package banco;

    /**
     *SeguroDeVida
     */
public class SeguroDeVida implements Tributavel {

    private Cliente titular;
    private int apolice;
    private double valor;
    
    public SeguroDeVida (Cliente titular, int apolice, double valor){
        
        this.titular = titular;
        this.setApolice(apolice);
        this.valor = valor;
    }
    
    /**************  METODOS  ***************/ 
    //faixa fixa de R$500 + 2% do valor do seguro
    public double getValorImposto() {
        return 500 + this.valor * 0.02;
    }
    
    /**************  GETS E SETS  ***************/
    
    public void setApolice(int apolice) {
        this.apolice = apolice;
    }
    
    public String getTipo() {
        return "Seguro de Vida";
    }
    
    public String getTitular() {
        return this.titular.toString();
    }
}
