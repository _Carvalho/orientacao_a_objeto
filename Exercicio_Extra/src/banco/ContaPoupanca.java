package banco;

    /*
    *ContaPoupança
    */
public class ContaPoupanca extends Conta{
    
    private Cliente titular;
    
    public ContaPoupanca(Cliente titular, int numero, String agencia, String dataAbertura) {
        super(titular, numero, agencia, dataAbertura);
    }   
    /**************  METODOS  ***************/    
    public double calcularRendimento(int periodo) {
        return (Math.pow(1.07, periodo) - 1);
    }

    public double calcularTributacao(int periodo) {
        return 0;
    }
    
    /**************  GETS E SETS  ***************/    
    public String getTitular() {
        return this.titular.toString();
    }
    
    public String getTipo() {
        return "Conta Poupanca";
    }
    
}
