
package banco;
/**
 * Cliente
 */
public class Cliente {
    
    private String primeiroNome;
    private String ultimoNome;
    private String cpf;
    
    public Cliente (String primeiroNome, String ultimoNome,String cpf){
        this.primeiroNome = primeiroNome;
        this.ultimoNome = ultimoNome;
        this.cpf = cpf;
    }

    /**************  METODOS  ***************/    

    public String toString() {
        return "Nome: " + this.primeiroNome + " " + this.ultimoNome + " e CPF: " + this.cpf;
    }
    
      public void ativos(ContaCorrente contaCor, ContaPoupanca contaPoup, SeguroDeVida seguroS) {
        Object[] ativos = new Object[3];
        ativos[0] = contaCor;
        ativos[1] = contaPoup;
        ativos[2] = seguroS;
        
        for (Object ativo : ativos) {
            if(ativo instanceof Tributavel){
                System.out.println(((Tributavel) ativo).getTipo() + ": Imposto devido: " + ((Tributavel) ativo).getValorImposto());
            } else {
                System.out.println(((Conta) ativo).getTipo() + ": Ativo não tributável.");
            }
        }
    }
    
}
