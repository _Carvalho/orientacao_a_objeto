package banco;
/**
 * ContaCorrente
 */
public class ContaCorrente extends Conta implements Tributavel {
   
    private Cliente titular;
    
    public ContaCorrente(Cliente titular, int numero, String agencia, String dataAbertura) {
        super(titular, numero, agencia, dataAbertura);
    }
    
    /**************  METODOS  ***************/ 
    public double calcularRendimento(int periodo) {
        return (Math.pow(1.1, periodo) - 1);
    }
     
    public double calcularTributacao(int periodo) {
        return calcularRendimento(periodo) * 0.15;
    }
    
    /**************  GETS E SETS  ***************/    
    public String getTitular() {
        return this.titular.toString();
    }
    
    public String getTipo(){
        return "Conta Corrente";
    }
    
    //15% ao ano sobre o saldo
    public double getValorImposto() {
        return super.getSaldo() * 0.15;
    }
        
}
    

