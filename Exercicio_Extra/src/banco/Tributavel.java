package banco;

/**
 * Tributavel
 */
public interface Tributavel {

/**************  METODOS  ***************/ 
    public double getValorImposto();
    
/**************  GETS E SETS  ***************/
    public String getTitular();
    public String getTipo();   
}
